FROM debian:jessie
LABEL "SHORT"           "Debian Jessie with go installed from downloaded binaries"
ENV   "go_version"      "1.7.3"
LABEL "go.version"      ${go_version}

# A highly subjective interpretation of developers tools
RUN export DEBIAN_FRONTEND='noninteractive' && \
    apt-get update  && \
    apt-get upgrade -y && \
    apt-get install -y \
      screen \
      git mercurial python-dulwich \
      wget rsync \
      net-tools coreutils \
      make \
      gcc binutils \
      emacs xauth

RUN mkdir dist
ADD https://storage.googleapis.com/golang/go${go_version}.linux-amd64.tar.gz dist/
RUN tar -C /usr/local -vxz -f dist/go${go_version}.linux-amd64.tar.gz

RUN mkdir "$HOME/go"
RUN echo "export GOROOT=/usr/local/go" >> ~/.bashrc
RUN echo "PATH=$PATH:$GOROOT/bin" >> ~/.bashrc
RUN echo "export GOPATH=$HOME/go" >> ~/.bashrc

RUN ln /usr/local/go/bin/go /usr/local/bin/go

CMD while true; do sleep 1; done
